package studenty;

import java.util.HashMap;
import java.util.Map;

public class Main {
    public static void main(String[] args) {
        University university = new University();

        Student student = new Student("a", 1L, "n");
        Student student1 = new Student("a1", 100200L, "n1");
        Student student2 = new Student("a2", 3L, "n2");
        Student student3 = new Student("a3", 4L, "n3");
        Student student4 = new Student("a4", 100400L, "n4");

        university.addStudent(student);
        university.addStudent(student2);
        university.addStudent(student3);
        university.addStudent(student4);
        university.addStudent(student1);

        System.out.println("Studiuje: " + university.containsStudent(100200));
        System.out.println("Ziomek: " + university.getStudent(100200));
        university.printAllStudent();

//        mapa.put(student.getIndeks(), student );
//        mapa.put(student1.getIndeks(), student1 );
//        mapa.put(student2.getIndeks(), student2 );
//        mapa.put(student3.getIndeks(), student3 );
//        mapa.put(student4.getIndeks(), student4 );
//
//        System.out.println("Czy zawiera: " + );
//        System.out.println("100400 : " + );
//        System.out.println("Rozmiar:  " + mapa.size()); // liczba studentow
//
//        // opcja 1
//        for (Student iterowanyStudent: mapa.values()) {
//            System.out.println(iterowanyStudent);
//        }
//        // opcja 2




    }
}
