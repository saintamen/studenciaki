package studenty;

import java.util.HashMap;
import java.util.Map;

public class University {
    private Map<Long, Student> mapa = new HashMap<>();

    public University() {
    }

    public void addStudent(long indexNumber, String name, String surname){
        Student student = new Student(name, indexNumber, surname);

        mapa.put(indexNumber, student);
    }

    public void addStudent(Student student){
        mapa.put(student.getIndeks(), student);
    }

    public boolean containsStudent(long indexNumber) {
        return mapa.containsKey(indexNumber);
    }

    public Student getStudent(long indexNumber){
        return mapa.get(indexNumber);
    }

    public int studentsCount(){
        return mapa.size();
    }

    public void printAllStudent(){
        System.out.println(mapa.values());
    }

//    int studentsCount()
//    Student getStudent(long indexNumber)
//    boolean containsStudent(long indexNumber)d
//    void addStudent(long indexNumber, String name, String surname)
//    void printAllStudent()
}
